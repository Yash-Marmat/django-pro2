from django.shortcuts import render   # use for functions
from .models import Post
from django.views.generic import (  # use for classes (remember ListView is django's module)
    ListView, 
    DetailView,  
)


class BlogView(ListView):
    model         = Post
    template_name = 'home.html'

class BlogsDetail(DetailView):
    model         = Post
    template_name = 'post_detail.html'
