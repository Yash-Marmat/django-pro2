from django.urls import path
from .views import BlogView, BlogsDetail

urlpatterns = [
    path('post/<int:pk>/', BlogsDetail.as_view(), name = 'post_detail'),
    path('', BlogView.as_view(), name = 'home'),
]