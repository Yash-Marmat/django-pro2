# username for this models is yash2

from django.db import models


class Post(models.Model):  # so that our model can work on every function present in django models
    title  = models.CharField(max_length = 200)
    body   = models.TextField()
    author = models.ForeignKey(
        'auth.user',
        on_delete = models.CASCADE,
    )

    def __str__(self):
        return self.title


# summary:

'''
At the top we’re importing the class models and then creating a subclass of models.Model
called Post. Using this subclass functionality we automatically have access to everything within django.db.models.Models and can add additional fields and methods as
desired.

For title we’re limiting the length to 200 characters and for body we’re using a
TextField which will automatically expand as needed to fit the user’s text. There are
many field types available in Django; you can see the full list here.

For the author field we’re using a ForeignKey which allows for a many-to-one relationship. This means that a given user can be the author of many different blog posts
but not the other way around. The reference is to the built-in User model that Django
provides for authentication. For all many-to-one relationships such as a ForeignKey
we must also specify an on_delete option.
'''